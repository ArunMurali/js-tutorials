# JavaScript Programs
    ES6 concepts in JS

## Table of Content

* [Basic literals ](./src/1_basic_literals.js)
* [constructor ](./src/2_constructor.js)
* [prototype](./src/3_prototype.js)
* [Inheritance](./src/4_inheritance.js)
* [Object Create](./src/5_object_create.js)
* [Classes](./src/6_classes.js)
* [SubClasses](./src/7_subclasses.js)
* [Map and Set](./src/8_map_set.js)
* [Promise Object](./9.2_promise_Object.js)
* [Decorators](./src/14_decorators.js)

## Description

### [__this__ keyword](./src/14_this_keyword.js)
- __this__ keyword refer to the object that the function is a keyword. the value of the this keyword will always depends on the object that invoking the function.
### [Promise Object](./src/9.2_promise_Object.js)
* A JavaScript Promise object can be:
    - Pending
    - Fulfilled
    - Rejected
* The Promise object supports two properties: state and result.
* While a Promise object is "pending" (working), the result is undefined.
* When a Promise object is "fulfilled", the result is a value.
* When a Promise object is "rejected", the result is an error object.
### [Decorator](./src/14_decorators.js)
Decorators are the way of wrapping one piece of code with another or apply a wrapper around a function in JavaScript. 

## run the js file
- In `webpack.config.js` file, change  entry point to the specific file which needs to be run
- eg:  12_this_keyword.js ->  entry: path.resolve(__dirname, "./src/12_this_keyword.js"),
 ```
 run npm dev
 ```
- Page will be running in the port http://localhost:8080/


