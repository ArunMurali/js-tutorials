// call 
const object1 = {
    name: "Arun",
    getName: function () {
      console.log(this.name);
    },
  };
  const object2 = {
    name: "Varun",
  };
  object1.getName.call(object2);
  
  // bind
  function getBikeDetails(model, company) {
   console.log(this.name + " " + model + " " + company) 
  }
  const bike = {
    name: "bUllet",
  };

  getBikeDetails.bind(bike, "standard", "royal enfield")();
  
  // apply
  getBikeDetails.apply(bike, [ "standard", "royal enfield"]);
  