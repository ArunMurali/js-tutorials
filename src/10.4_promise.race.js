const promise1 = 4; // Promise resolved
const promise3 = new Promise((resolve, reject) => {
  // Promise rejected
  setTimeout(() => {
    reject("Not fulfilled");
  }, 1000);
});
const promise2 = new Promise((resolve, reject) => {
  // Promise resolved
  setTimeout(() => {
    resolve("Promise resolved");
  }, 1000);
});

/**
 * Promise.race
 * first fulfilled promise is return either it is resolved or rejected
 */
//

Promise.race([promise2, promise3])
  .then((res) => {
    console.log(res);
  })
  .catch((error) => console.error(error));

/**

 */
