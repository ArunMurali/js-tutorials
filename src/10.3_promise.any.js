const promise1 = 4; // Promise resolved
const promise2 = new Promise((resolve, reject) => {
  // Promise resolved
  return resolve("Promise resolved");
});
const promise3 = new Promise((resolve, reject) => {
  // Promise rejected
  return reject("Not fulfilled");
});

/**
 * Promise.any
 * it return first resolved promise
 * if all promise is rejected it return aggregated error
 */
//

Promise.any([promise1, promise2]).then((res) => {
  console.log(res);
});

/**
 * output
 *  4
 * 
 */
Promise.any([promise3])
  .then((res) => {
    console.log(res);
  })
  .catch((onrejected) => {
    console.log(onrejected);
  });
  
  /**
 * Output:-  
 * 
 * return first resolved result if all are rejected returns
 * AggregateError: All promises were rejected
 * 
 */
