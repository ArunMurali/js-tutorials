class Book {
  constructor(title, author, year) {
    this.title = title;
    this.year = year;
    this.author = author;
  }
  getSummary() {
    return `${this.title} written by ${this.author} in the year ${this.year}`;
  }
}

// Magazine subclass
class Magazine extends Book {
  constructor(title, author, year, month) {
    super(title, author, year);
    this.month = month;
  }
}

// instantiate an object
const magi= new Magazine("Book one", "John doe", "2013", "March");
console.log(magi)