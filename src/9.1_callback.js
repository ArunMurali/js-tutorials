const square = (sq) => {
  return sq * sq;
};
const sum = (x, y, callback) => {
  return callback(x + y);
};
const sumSquare = sum(1, 2, square);
console.log(sumSquare)
