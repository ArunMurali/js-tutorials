
// Object literal

const book1 = {
    title:'Book one',
    author: 'John Doe',
    year: '2013',
    getSummary: function () {
        return `${this.title} written by ${this.author} in the year ${this.year}`
    }
}
const book2 = {
    title:'Book two',
    author: 'Mark antony',
    year: '2014',
    getSummary: function () {
        return `${this.title} written by ${this.author} in the year ${this.year}`
    }
}
const root = document.getElementById('root')
const li1 = document.createElement('li');
li1.innerHTML = book1.getSummary()
const li2 = document.createElement('li');
li2.innerHTML = book2.getSummary()
root.appendChild(li1);
root.appendChild(li2);

console.log(book1.getSummary())
console.log(book2.getSummary())