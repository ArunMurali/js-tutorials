//constructor
function Book(title, author, year) {
  this.title = title;
  this.author = author;
  this.year = year;
}
Book.prototype.getSummary = function () {
  return `${this.title} written by ${this.author} in the year ${this.year}`;
};


// Magazine need to inherit the properties of the book
function Magazine(title, author, year, month) {
    Book.call(this, title, author, year);
    this.month = month;
  }
  // Inherit prototype
  Magazine.prototype = Object.create(Book.prototype)
    
  // Instantiate Object
  const mag1 = new Magazine('Book one', 'John doe', '2013', 'march');
  console.log(mag1.getSummary())

  // use magazine constructor
  Magazine.prototype.constructor = Magazine;