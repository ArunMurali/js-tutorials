const bookProtos = {
  getSummary: function () {
    return `${this.title} written by ${this.author} in the year ${this.year}`;
  },
  getAge: function () {
    const years = new Date().getFullYear() - this.year;
    return `this ${this.title} is ${years} years old`;
  },
};

//create Object
// const book1 = Object.create(bookProtos)
// book1.title = 'Book one'
// book1.author = 'John Doe'
// book1.year = '2013'
// console.log(book1.getAge())

const book1 = Object.create(bookProtos, {
  title: { value: "Book one" },
  author: { value: "John Doe" },
  year: { value: "2013" },
});
console.log(book1.getAge())
