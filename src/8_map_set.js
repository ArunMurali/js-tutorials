// map using map constructor
const map1 = new Map();
map1.set(1, "one");
map1.set(2, "two");
console.log(map1, "map 1");
const map2 = new Map([
  [1, "one"],
  [2, "two"],
]);
console.log(map2, "map 2");


// set using set contractor
const set1 = new Set();
set1.add(1);
set1.add(2);
console.log(set1, "set1");
const set2 = new Set([1, 2, 3, 4]);
console.log(set2, "set2");
console.log(set2.has(1),"set2 has 1")
console.log(set2.keys())
