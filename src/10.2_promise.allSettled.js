const promise1 = 4; // Promise resolved
const promise2 = new Promise((resolve, reject) => {
  // Promise resolved
  return resolve("Promise resolved");
});
const promise3 = new Promise((resolve, reject) => {
  // Promise rejected
  return reject("Not fulfilled");
});

/**
 * Promise.allSettled
 * all promise will be fulfilled with status resolved or rejected
 */
//

Promise.allSettled([promise1, promise3])
  .then((res) => {
    console.log(res);
  })
  .catch((error) => {
    console.log(error);
  });
/**
 * [
 *  {
 *       "status": "fulfilled",
 *       "value": 4
 *   },
 *   {
 *       "status": "rejected",
 *       "reason": "Not fulfilled"
 *   }
 *  ]
 * 
 */

  
 
