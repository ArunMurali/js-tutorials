class Book {
  constructor(title, author, year) {
    this.title = title;
    this.year = year;
    this.author = author;
  }
  #name = "arun"
  getSummary() {
    return `${this.title} written by ${this.author} in the year ${this.year}`;
  }
}
const root = document.getElementById('root');



// Instantiate object
const book1 = new Book('Book one', 'John Doe', '2013')
root.innerHTML = JSON.stringify(book1);

console.log(book1)