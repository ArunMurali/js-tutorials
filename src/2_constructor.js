
//constructor
function Book(title, author, year) {
 this.title= title;
 this.author = author;
 this.year = year;
 this.getSummary = function () {
    return `${this.title} written by ${this.author} in the year ${this.year}`
}

}

//Instantiate an object
const book1 = new Book('Book one', 'John doe', '2013');
const book2 = new Book('Book two', 'Mark Antony', '2014');
console.log(book1.getSummary())

