const promise1 = 4; // Promise resolved
const promise2 = new Promise((resolve, reject) => {
  // Promise resolved
  return resolve("Promise resolved");
});
const promise3 = new Promise((resolve, reject) => {
  // Promise rejected
  return reject("Not fulfilled");
});

/**
 * Promise.all
 * if any one of the promise is not resolved it throws an error
 */
//

Promise.all([promise1, promise2]).then((res) => {
  console.log(res);
});

/**
 * output
 *  [
 *  4,
 *  "Promise resolved"
 *  ]
 * 
 */
Promise.all([promise1, promise3])
  .then((res) => {
    console.log(res);
  })
  .catch((onrejected) => {
    console.log(onrejected);
  });
  
  /**
 * throws error because one of the promise is not resolved
 * output
 * Not fulfilled
 * 
 */
